Resources used in ListGig
=====

PHP Framework: Slim Framework - http://www.slimframework.com/

Templating Engine: Twig - http://twig.sensiolabs.org/

Extras:
==
 *   Slim Views - https://github.com/codeguy/Slim-Views
 *   BootStrap 3 - http://getbootstrap.com/
 *   Simplex BootStrap Theme, from Bootswatch - http://bootswatch.com/
 *   jQuery - http://
 *   Google Maps Javascript API v3 - https://developers.google.com/maps/documentation/javascript/
 *   The Final Countdown for jQuery - http://hilios.github.io/jQuery.countdown/
 *   moment.js - http://momentjs.com/
 *   bootstrap-datetimepicker - https://github.com/Eonasdan/bootstrap-datetimepicker
 *   handlebars.js - http://handlebarsjs.com/
 *   typeahead.js - http://twitter.github.io/typeahead.js/