<?php

$app->hook('slim.before.dispatch', function() use ($app) {
    $user = null;
    if (isset($_SESSION['user'])) {
        $user = $_SESSION['user'];
    }

    $app->view()->setData('siteroot', $app->config('siteroot'));
    $app->view()->setData('user', $user);

    // PHP 5.3 and under doesn't support Function array dereferencing (new feature within 5.4)
    // Request to Uni C&IT, Please update to at least 5.4, if not 5.5. This was not the only
    // part of my application that I had to modify because University servers were behind on
    // their PHP version.
    $environ = $app->environment();
    $app->view()->setData('path', $environ['PATH_INFO']);
    $app->view()->setData('method', $environ['REQUEST_METHOD']);
});

$app->notFound(function() use ($app) {
    $app->render('404.html', array(), 404);
});

$app->get("/", function() use ($app) {
    $app->render('index.html');
});

$app->get("/search", function() use ($app) {
	$app->render('search.html');
});

$app->post("/search", function() use ($app) {
	$app->response->headers->set('Content-Type', 'application/json');
	include("views/searcher.php");
});

$app->get('/acts(/:letter)', function($letter='A') use ($app) {
    include('views/acts.php');
});

$app->get('/act(/:act)', function($act=NULL) use ($app) {
	if ($act) {
		include('views/act.php');
	} else {
		$app->redirect($app->config('siteroot') . '/acts');
	}
});

$app->get('/venues(/:letter)', function($letter='A') use ($app) {
    include('views/venues.php');
});

$app->get('/venue(/:venue)', function($venue=NULL) use ($app) {
	if ($venue) {
		include('views/venue.php');
	} else {
		$app->redirect($app->config('siteroot') . '/venues');
	}
});

$app->get('/gig/:gig', function($gig) use ($app) {
	include('views/gig.php');
});

$app->get('/login', function() use ($app) {
	if (!isset($_SESSION['user'])) {
		$app->render('login.html');
	} else {
		$app->flash('type', 'info');
		$app->flash('message', 'You are already logged in.');
		$app->redirect($app->config('siteroot') . '/');
	}
});

$app->post('/login', function() use ($app) {
	include('views/login.php');
});

$app->get('/logout', function() use ($app) {
	unset($_SESSION['user']);
	$app->view()->setData('user', null);
	$app->flash('type', 'info');
	$app->flash('message', 'Successfully Logged Out.');
	$app->redirect($app->config('siteroot') . '/');
});


// ==================== Administration Routing ====================


$app->get('/admin', $authenticate($app), function() use ($app) {
	$app->render('admin.html');
});

$app->get('/admin/acts(/:number)', $authenticate($app), function($number=1) use ($app) {
	include('views/admin/acts.php');
});

$app->post('/admin/act/update/:id', $authenticate($app), function($id) use ($app) {
	include('views/admin/act_update.php');
});

$app->get('/admin/venues(/:number)', $authenticate($app), function($number=1) use ($app) {
	include('views/admin/venues.php');
});

$app->post('/admin/venue/update/:id', $authenticate($app), function($id) use ($app) {
	include('views/admin/venue_update.php');
});

$app->get('/admin/gigs(/:number)', $authenticate($app), function($number=1) use ($app) {
	include('views/admin/gigs.php');
});

$app->get('/admin/gig/create', $authenticate($app), function() use ($app) {
	$app->render('admin_gig_create.html');
});

$app->post('/admin/gig/create', $authenticate($app), function() use ($app) {
	include('views/admin/gig_create.php');
});

$app->post('/admin/gig/update/:id', $authenticate($app), function($id) use ($app) {
	include('views/admin/gig_update.php');
});

$app->get('/admin/users(/:number)', $authenticate($app), function($number=1) use ($app) {
	include('views/admin/users.php');
});


// ==================== API Routing ====================

$app->get('/api', function() use ($app) {
    $app->render('api.html');
});

$app->get('/api/json/act/:act', function($act) use ($app) {
	$app->response->headers->set('Content-Type', 'application/json');
	include("views/api/act.php");
});

$app->get('/api/json/spotlight', function() use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');
    include("views/api/spotlight.php");
});

$app->get('/api/json/upcoming(/:act)(/:metro)', function($act=NULL, $metro=NULL) use ($app) {
    $app->response->headers->set('Content-Type', 'application/json');
    include("views/api/upcoming.php");
});

$app->get('/api/json/acts/list', function() use ($app) {
	$app->response->headers->set('Content-Type', 'application/json');
	include('views/api/list_acts.php');
});

$app->get('/api/json/venues/list', function() use ($app) {
	$app->response->headers->set('Content-Type', 'application/json');
	include('views/api/list_venues.php');
});