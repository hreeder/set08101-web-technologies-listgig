<?php
require 'vendor/autoload.php';
require 'config.php';

if ($config['mode'] == 'development') {
    ini_set('display_startup_errors',1);
    ini_set('display_errors',1);
    error_reporting(-1);
}

require_once('classes/User.php');
require_once('classes/utils.php');

$app = new \Slim\Slim($config);
$app->add(new \Slim\Middleware\SessionCookie($config['sessionCookie']));
$app->add(new \Slim\Middleware\Flash());

include 'twig.php';
include 'routes.php';

$app->run();
?>
