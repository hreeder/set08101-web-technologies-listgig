<?php

$view = $app->view(new \Slim\Views\Twig());
$view->parserOptions = array(
    'debug' => true,
    // 'cache' => dirname(__FILE__) . '/cache'
);
$view->parserExtensions = array(
    new \Slim\Views\TwigExtension(),
);