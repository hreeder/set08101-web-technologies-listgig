<?php
if (!isset($_SESSION['user'])) {
	global $db;
	
	$user = $app->request()->post('user');
	$pass = $app->request()->post('passwd');
	
	if (isset($user) && isset($pass)) {
		if (User::checkCredentials($user, $pass)) {
			$_SESSION['user'] = $user;
			$app->redirect($app->config('siteroot') . '/');
		} else {
			$app->flash('type', 'warning');
			$app->flash('message', 'Those credentials were incorrect. Please try again');
			$app->redirect($app->config('siteroot') . '/login');
		}
	}
} else {
	$app->flash('type', 'info');
	$app->flash('message', 'You are already logged in.');
	$app->redirect($app->config('siteroot') . '/');
}