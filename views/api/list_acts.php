<?php
global $db;
try {
    $conn = new PDO($db['dsn'], $db['user'], $db['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
	$stmt = $conn->prepare('SELECT id, sname FROM act');
	$stmt->execute();

    $acts = array();

    while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
    	$acts[] = array(
    		'id' => $row->id,
    		'name' => $row->sname
    	);
    }
 
    print json_encode($acts);

} catch(PDOException $e) {
    $app->flash('danger', 'Database Error: ' . $e->getMessage());
    $app->render('error.html');
}
