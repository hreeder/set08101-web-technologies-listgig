<?php
global $db;
try {
    $conn = new PDO($db['dsn'], $db['user'], $db['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    if ($act) {
        $stmt = $conn->prepare('SELECT g.id, g.start, g.sname title, v.sname venuename, v.metro, a.sname act FROM gig g RIGHT JOIN venue v ON g.venue=v.id JOIN act a ON a.id=g.act WHERE g.act = :act AND g.start > NOW() ORDER BY start ASC');
        $stmt->execute(array('act' => $act));
    } else {
        $stmt = $conn->prepare('SELECT g.id, g.start, g.sname title, v.sname venuename, v.metro, a.sname act FROM gig g RIGHT JOIN venue v ON g.venue=v.id JOIN act a ON a.id=g.act WHERE g.start > NOW() ORDER BY start ASC LIMIT 5');
        $stmt->execute();
    }

    $gig = array();

    while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
        $gig[$row->id] = array(
            'id' => $row->id,
            'start' => $row->start,
            'title' => $row->title,
            'venue' => $row->venuename,
	        'act' => $row->act,
            'metro' => $row->metro
        );
    }
 
    print json_encode($gig);

} catch(PDOException $e) {
    $app->flash('danger', 'Database Error: ' . $e->getMessage());
    $app->render('error.html');
}
