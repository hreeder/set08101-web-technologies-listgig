<?php
global $db;
try {
    $conn = new PDO($db['dsn'], $db['user'], $db['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
	$stmt = $conn->prepare('SELECT id, sname, metro FROM venue');
	$stmt->execute();

    $venues = array();

    while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
    	$venues[] = array(
    		'id' => $row->id,
    		'name' => $row->sname,
    		'metro' => $row->metro
    	);
    }
 
    print json_encode($venues);

} catch(PDOException $e) {
    $app->flash('danger', 'Database Error: ' . $e->getMessage());
    $app->render('error.html');
}
