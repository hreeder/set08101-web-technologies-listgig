<?php
global $db;
try {
	$conn = new PDO($db['dsn'], $db['user'], $db['pass']);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$stmt = $conn->prepare('SELECT id, sname, txt, img FROM act ORDER BY RAND() LIMIT 1');
    $stmt->execute();

    $act = $stmt->fetch(PDO::FETCH_OBJ);
    print json_encode(array(
    	'id' => $act->id,
    	'name' => $act->sname,
    	'desc' => $act->txt,
    	'img' => $act->img
    ));
} catch(PDOException $e) {
	$app->flash('danger', 'Database Error: ' . $e->getMessage());
	$app->render('error.html');
}