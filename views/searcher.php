<?php
global $db;
$data = $app->request()->post();
if ($data['searchtext']) {
	$results = array();
	// DO SEARCHING THINGS HERE

	$conn = new PDO($db['dsn'], $db['user'], $db['pass']);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$stmt = $conn->prepare("SELECT id,sname,type FROM ((SELECT id, sname, 'act' as type FROM act WHERE sname LIKE :searchterm LIMIT 6) UNION (SELECT id, sname, 'gig' as type FROM gig WHERE sname LIKE :searchterm LIMIT 6) UNION (SELECT id, sname, 'venue' as type FROM venue WHERE sname LIKE :searchterm LIMIT 6)) AS result_table");
	$stmt->execute(array('searchterm' => '%' . $app->request->post('searchtext') . '%'));

	$results = array();

	while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
		$results[] = array(
			'id' => $row->id,
			'text' => htmlentities($row->sname, ENT_HTML5, 'ISO-8859-15'),
			'type' => $row->type
		);
	}
	
	print json_encode($results);
}
