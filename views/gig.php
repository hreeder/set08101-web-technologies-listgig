<?php
global $db;
try {
    $conn = new PDO($db['dsn'], $db['user'], $db['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $gigid = $gig;
    $stmt = $conn->prepare('SELECT g.sname, g.start, g.img, a.sname aname, a.img aimg, a.id actid, v.sname vname, v.img vimg, v.lat, v.lng, v.metro, v.id venueid FROM gig g JOIN act a ON g.act=a.id JOIN venue v ON g.venue=v.id WHERE g.id = :gigid');
    $stmt->execute(array('gigid' => $gig));

    $gig = $stmt->fetch(PDO::FETCH_OBJ);

    $app->render('gig.html',
        array(
            'id' => $gigid,
            'name' => $gig->sname,
            'start' => $gig->start,
            'img' => $gig->img,
            'act' => array(
                    'id' => $gig->actid,
                    'name' => $gig->aname,
                    'img' => $gig->aimg,
                ),
            'venue' => array(
                    'id' => $gig->venueid,
                    'name' => $gig->vname,
                    'img' => $gig->vimg,
                    'lat' => $gig->lat,
                    'lng' => $gig->lng,
                    'metro' => $gig->metro,
                )
        )
    );
} catch(PDOException $e) {
    $app->flash('type', 'danger');
    $app->flash('message', 'Database Error: ' . $e->getMessage());
    $ref = $app->request()->getReferer();
    if ($ref) {
        $app->redirect($ref);
    } else {
        $app->redirect($app->config('siteroot') . '/acts');
    }
}
