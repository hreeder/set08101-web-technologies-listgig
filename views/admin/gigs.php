<?php
try {
	global $db;

    $conn = new PDO($db['dsn'], $db['user'], $db['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare('SELECT COUNT(*) FROM gig');
    $stmt->execute();

    $count = $stmt->fetch(PDO::FETCH_NUM);
    $count = $count[0];

    $perPage = 25;
    $upper = ($number * $perPage) - 1;
    $lower = $upper - $perPage;
    if ($lower < 0) {
    	$lower = 0;
    }

	$stmt = $conn->prepare('SELECT g.id, g.start, a.sname aname, v.sname vname FROM gig g JOIN act a ON g.act=a.id JOIN venue v ON g.venue=v.id ORDER BY g.id ASC LIMIT :lower, :upper');
	$stmt->bindParam(':lower', $lower, PDO::PARAM_INT);
	$stmt->bindParam(':upper', $upper, PDO::PARAM_INT);
	$stmt->execute();

	$gigs = array();

	while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
		$gigs[$row->id] = array(
			'start' => $row->start,
			'act' => $row->aname,
			'venue' => $row->vname
		);
	}

	$app->render('admin_gigs.html', array(
		'perpage' => $perPage,
		'curr' => $number,
		'count' => $count,
		'gigs' => $gigs
	));
} catch(PDOException $e) {
    $app->flash('type', 'danger');
    $app->flash('message', 'Database Error: ' . $e->getMessage());
    $ref = $app->request()->getReferer();
    if ($ref) {
        $app->redirect($ref);
    } else {
        $app->redirect($app->config('siteroot') . '/admin');
    }

}