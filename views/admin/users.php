<?php
try {
	global $db;

    $conn = new PDO($db['dsn'], $db['user'], $db['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$stmt = $conn->prepare('SELECT userid, username FROM wt_users ORDER BY userid ASC');
	$stmt->bindParam(':lower', $lower, PDO::PARAM_INT);
	$stmt->bindParam(':upper', $upper, PDO::PARAM_INT);
	$stmt->execute();

	$users = array();

	while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
		$users[$row->userid] = array('name' => $row->username);
	}

	$app->render('admin_users.html', array(
		'users' => $users
	));
} catch(PDOException $e) {
    $app->flash('type', 'danger');
    $app->flash('message', 'Database Error: ' . $e->getMessage());
    $ref = $app->request()->getReferer();
    if ($ref) {
        $app->redirect($ref);
    } else {
        $app->redirect($app->config('siteroot') . '/admin');
    }

}