<?php
global $db;
try {
	$environ = $app->environment();
	$method = $environ['REQUEST_METHOD'];

	// print_r($_POST);

	$gigName = $app->request->post('gigName');
	$start = $app->request->post('start');
	$img = $app->request->post('imgUrl');
	$actName = $app->request->post('act');
	$venueName = $app->request->post('venue');

	$format = DateTime::createFromFormat("d/m/Y G:i", $start);
	$startTS = $format->getTimestamp();

	// print $start;

	$conn = new PDO($db['dsn'], $db['user'], $db['pass']);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$stmt = $conn->prepare('INSERT INTO gig(`start`, `sname`, `venue`, `act`, `img`) VALUES (:start, :name, (SELECT id FROM venue WHERE sname = :venue), (SELECT id FROM act WHERE sname = :act), :img)');
	$stmt->execute(array(
		'start' => date("Y-m-d H:i:s", $startTS),
		'name' => $gigName,
		'venue' => $venueName,
		'act' => $actName,
		'img' => $img
	));

	$app->redirect($app->config('siteroot') . '/admin/gigs');
} catch(PDOException $e) {
    $app->flash('type', 'danger');
    $app->flash('message', 'Database Error: ' . $e->getMessage());
    $ref = $app->request()->getReferer();
    if ($ref) {
        $app->redirect($ref);
    } else {
        $app->redirect($app->config('siteroot') . '/admin/gigs');
    }

}