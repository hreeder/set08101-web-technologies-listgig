<?php
try {
	global $db;

    $conn = new PDO($db['dsn'], $db['user'], $db['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare('UPDATE gig SET sname=:name, start=:start, img=:img WHERE id=:id');

    $gigDT = $app->request->post('start');
    $gigDT = preg_split("/[\/\:\s]+/", $gigDT);

    $stmt->execute(array(
        'name' => $app->request->post('name'),
        'start' => $gigDT[2] . "-" . $gigDT[1] . "-" . $gigDT[0] . " " . $gigDT[3] . ":" . $gigDT[4] . ":00",
        'img' => $app->request->post('imgurl'),
        'id' => $id
    ));

    $app->redirect($app->config('siteroot') . $_POST['prev']);

} catch(PDOException $e) {
    $app->flash('type', 'danger');
    $app->flash('message', 'Database Error: ' . $e->getMessage());
    $ref = $app->request()->getReferer();
    if ($ref) {
        $app->redirect($ref);
    } else {
        $app->redirect($app->config('siteroot') . '/admin');
    }

}