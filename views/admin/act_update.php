<?php
try {
	global $db;
    $environ = $app->environment();
	$method = $environ['REQUEST_METHOD'];

    $conn = new PDO($db['dsn'], $db['user'], $db['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare('UPDATE act SET sname=:name, txt=:txt, img=:img WHERE id=:id');
    $stmt->execute(array('name' => $app->request->post('name'), 'txt' => $app->request->post('desc'), 'img' => $app->request->post('imgurl'), 'id' => $id));

    $app->redirect($_POST['prev']);

} catch(PDOException $e) {
    $app->flash('type', 'danger');
    $app->flash('message', 'Database Error: ' . $e->getMessage());
    $ref = $app->request()->getReferer();
    if ($ref) {
        $app->redirect($ref);
    } else {
        $app->redirect($app->config('siteroot') . '/admin');
    }

}