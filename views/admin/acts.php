<?php
try {
	global $db;

    $conn = new PDO($db['dsn'], $db['user'], $db['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare('SELECT COUNT(*) FROM act');
    $stmt->execute();

    $count = $stmt->fetch(PDO::FETCH_NUM);
    $count = $count[0];

    $perPage = 25;
    $upper = ($number * $perPage) - 1;
    $lower = $upper - $perPage;
    if ($lower < 0) {
    	$lower = 0;
    }

	$stmt = $conn->prepare('SELECT id, sname, txt, img FROM act ORDER BY id ASC LIMIT :lower, :upper');
	$stmt->bindParam(':lower', $lower, PDO::PARAM_INT);
	$stmt->bindParam(':upper', $upper, PDO::PARAM_INT);
	$stmt->execute();

	$acts = array();

	while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
		$acts[$row->id] = array(
			'name' => $row->sname,
			'txt' => $row->txt
		);
	}

	$app->render('admin_acts.html', array(
		'perpage' => $perPage,
		'curr' => $number,
		'count' => $count,
		'acts' => $acts
	));
} catch(PDOException $e) {
    $app->flash('type', 'danger');
    $app->flash('message', 'Database Error: ' . $e->getMessage());
    $ref = $app->request()->getReferer();
    if ($ref) {
        $app->redirect($ref);
    } else {
        $app->redirect($app->config('siteroot') . '/admin');
    }

}
