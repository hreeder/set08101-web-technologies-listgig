<?php
global $db;
try {
    $conn = new PDO($db['dsn'], $db['user'], $db['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $letter = strtoupper($letter);

    $stmt = $conn->prepare('SELECT DISTINCT LEFT(sname, 1) letter FROM venue WHERE LEFT(sname, 1) regexp "^[a-z]" ORDER BY letter ASC');
    $stmt->execute();

    $letters[] = '#';

    while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
        $letters[] = $row->letter;
    }

    if ($letter != 'NUM') {
        $likeletter = $letter . '%';
        $stmt = $conn->prepare('SELECT id, sname FROM venue WHERE sname LIKE :letter ORDER BY sname ASC');
        $stmt->execute(array('letter' => $likeletter));
    } else {
        $stmt = $conn->prepare('SELECT id, sname FROM venue WHERE LEFT(sname, 1) regexp "^[^a-z]" ORDER BY sname ASC');
        $stmt->execute();
        $letter = '#';
    }

    while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
        $venue[$row->id] = $row->sname;
    }

    $app->render('venues.html', array('letters' => $letters, 'letter' => $letter, 'venues' => $venue));
} catch(PDOException $e) {
    $app->flash('type', 'danger');
    $app->flash('message', 'Database Error: ' . $e->getMessage());
    $ref = $app->request()->getReferer();
    if ($ref) {
        $app->redirect($ref);
    } else {
        $app->redirect($app->config('siteroot') . '/acts');
    }

}
