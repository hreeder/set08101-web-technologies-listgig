<?php
global $db;
try {
    $conn = new PDO($db['dsn'], $db['user'], $db['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare('SELECT sname, txt, metro, lat, lng, img FROM venue WHERE id = :venue');
    $stmt->execute(array('venue' => $venue));

    $venueobj = $stmt->fetch(PDO::FETCH_OBJ);

    $stmt = $conn->prepare('SELECT g.id, g.start, g.sname title, a.sname act, a.id actid FROM gig g JOIN act a ON g.act=a.id WHERE g.venue = :venue AND g.start > NOW() ORDER BY start ASC');
    $stmt->execute(array('venue' => $venue));

    $gig = array();

    while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
        $gig[$row->id] = array(
            'start' => $row->start,
            'act' => $row->act,
            'title' => $row->title
        );
    }
 
    $app->render('venue.html', array('id' => $venue, 'name' => $venueobj->sname, 'txt' => $venueobj->txt, 'metro' => $venueobj->metro, 'lat' => $venueobj->lat, 'lng' => $venueobj->lng, 'img' => $venueobj->img, 'upcoming' => $gig));
} catch(PDOException $e) {
    $app->flash('type', 'danger');
    $app->flash('message', 'Database Error: ' . $e->getMessage());
    $ref = $app->request()->getReferer();
    if ($ref) {
        $app->redirect($ref);
    } else {
        $app->redirect($app->config('siteroot') . '/venues');
    }
}
