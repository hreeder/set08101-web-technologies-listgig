<?php
global $db;
try {
    $conn = new PDO($db['dsn'], $db['user'], $db['pass']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare('SELECT sname, txt, img FROM act WHERE id = :act');
    $stmt->execute(array('act' => $act));

    $band = $stmt->fetch(PDO::FETCH_OBJ);

    $stmt = $conn->prepare('SELECT g.id, g.start, g.sname title, v.sname venuename, v.metro FROM gig g RIGHT JOIN venue v ON g.venue=v.id WHERE g.act = :act AND g.start > NOW() ORDER BY start ASC');
    $stmt->execute(array('act' => $act));

    $gig = array();

    while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
        $gig[$row->id] = array(
            'start' => $row->start,
            'title' => $row->title,
            'venue' => $row->venuename,
            'metro' => $row->metro
        );
    }
 
    $app->render('act.html', array('id' => $act, 'name' => $band->sname, 'txt' => $band->txt, 'img' => $band->img, 'upcoming' => $gig));
} catch(PDOException $e) {
    $app->flash('type', 'danger');
    $app->flash('message', 'Database Error: ' . $e->getMessage());
    $ref = $app->request()->getReferer();
    if ($ref) {
        $app->redirect($ref);
    } else {
        $app->redirect($app->config('siteroot') . '/acts');
    }
}