function titleCase(string) {
    var first = string.substr(0, 1), rest = string.substr(1);
    return first.toUpperCase() + rest;
}

function lgDateDisplay(date) {
    var day = '', month = '', hours = '', minutes = '';

    if (date.getDate() < 10) {
        day += '0';
    }
    day += date.getDate();

    if (date.getMonth() < 10) {
        month += '0';
    }
    month += date.getMonth();

    if (date.getHours() < 10) {
        hours += '0';
    }
    hours += date.getHours();

    if (date.getMinutes() < 10) {
        minutes += date.getMinutes();
    }
    minutes += date.getMinutes();
    return day + '/' + month + '/' + date.getFullYear() + ' @ ' + hours + ':' + minutes;
}