function loadUpcoming(dest) {
	$.getJSON(
		'/~40052308/wtcw2/api/json/upcoming',
		function(data) {
			$(dest + ' > thead').append('<tr><th>Who</th><th>Where</th><th>When</th></tr>');
			for (e in data) {
				event = data[e];
                var t = event['start'].split(/[- :]/);
        		var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
				$(dest + ' > tbody').append('<tr><td><a href="/~40052308/wtcw2/gig/' + event['id'] + '">' + event['act'] + '</a></td><td>' + event['venue'] + '</td><td>' + lgDateDisplay(d) + '</td></tr>');
			}
		}
	);
}

function loadSpotlight(dest) {
	$.getJSON(
		'/~40052308/wtcw2/api/json/spotlight',
		function(data) {
			$(dest + ' > .panel-heading > h1').append(' - ' + data['name']);
			$(dest + ' > .panel-body').append('<a href="/~40052308/wtcw2/act/' + data['id'] + '"><img src="' + data['img'].replace(/\\(.)/mg, "$1") + '" class="spotlight"></a>');
		}
	);
}

function lgSearch (value) {
	if (value.length > 2) {
		$.ajax({
			type: "POST",
			url: "search",
			data: {
				'searchtext' : value
			},
			dataType: "json",
			success: function(msg){
				if (value==$('#searchbox').val()) {
					console.log(msg);
					$('#result-table > tbody > tr').each(function(){$(this).remove();});
					for (var item in msg) {
						console.log(msg[item]);
						$('#result-table > tbody').append('<tr><td>' + titleCase(msg[item]['type']) + '</td><td><a href="/' + msg[item]['type'] + '/' + msg[item]['id'] +'">' + msg[item]['text'] + '</a></td></tr>');
					}
					
					$('#results').removeClass('hidden');
				}
			}
		});
	}
}
