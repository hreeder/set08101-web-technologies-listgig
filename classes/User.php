<?php
class User {
	public static function checkCredentials($user, $pass) {
		global $db;
		$conn = new PDO($db['dsn'], $db['user'], $db['pass']);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare('SELECT userid, username FROM wt_users WHERE username = :user AND password = :pass');
		$stmt->execute(array('user' => $user, 'pass' => md5($pass)));

		if ($stmt->rowCount() == 1) {
			return True;
		} else {
			return False;
		}
	}
}
