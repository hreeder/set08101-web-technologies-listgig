<?php
$authenticate = function ($app) {
    return function () use ($app) {
        if (!isset($_SESSION['user'])) {
            $_SESSION['urlRedirect'] = $app->request()->getPathInfo();
            $app->flash('type', 'danger');
            $app->flash('message', 'You must authenticate before you can visit that page!');
            $app->redirect($app->config('siteroot') . '/login');
        }
    };
};